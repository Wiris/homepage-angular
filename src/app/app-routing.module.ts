import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VagasComponent } from './components/vagas/vagas.component';
import { ContatoComponent } from './components/contato/contato.component';
import { BlogComponent } from './components/blog/blog.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./components/vagas/vagas.module').then(m => m.VagasModule)
  },
  {
    path: 'contato',
    loadChildren: () => import('./components/contato/contato.module').then(m => m.ContatoModule)
  },
  {
    path: 'blog',
    loadChildren: () => import('./components/blog/blog.module').then(m => m.BlogModule)
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
