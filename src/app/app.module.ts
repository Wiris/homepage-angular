import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { VagasComponent } from './components/vagas/vagas.component';
import { CardComponent } from './components/card/card.component';
import { FormComponent } from './components/form/form.component';
import { ContatoComponent } from './components/contato/contato.component';
import { FooterComponent } from './components/footer/footer.component';
import { AppRoutingModule } from './app-routing.module';
import { BlogComponent } from './components/blog/blog.component';
import { CardsService } from './services/cards.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    VagasComponent,
    CardComponent,
    FormComponent,
    ContatoComponent,
    FooterComponent,
    BlogComponent
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [CardsService],
  bootstrap: [AppComponent]
})
export class AppModule {}
