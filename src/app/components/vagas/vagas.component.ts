import { Component, OnDestroy, OnInit } from '@angular/core';
import { CardsService } from '../../services/cards.service';
import { Card } from '../../models/Card';
import { map, Subscription } from 'rxjs';

@Component({
  selector: 'app-vagas',
  templateUrl: './vagas.component.html',
  styleUrls: ['./vagas.component.scss']
})
export class VagasComponent implements OnInit, OnDestroy {
  cards: Card[] = [];
  subscription: Array<Subscription> = [];
  constructor(private cardService: CardsService) {}

  ngOnInit(): void {
    this.obterCards();
  }
  ngOnDestroy(): void {
    this.subscription.forEach(sub => sub.unsubscribe());
  }
  obterCards() {
    const sub = this.cardService
      .obterVagas()
      .pipe(map(r => r.data))
      .subscribe(response => {
        this.cards = response;
      });
    this.subscription.push(sub);
  }
}
