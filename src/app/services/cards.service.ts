import { Injectable } from '@angular/core';
import { Card } from '../models/Card';
import { HttpClient } from '@angular/common/http';
import { Data } from '../models/Data';
import { Observable } from 'rxjs';
@Injectable()
export class CardsService {
  constructor(private http: HttpClient) {}

  obterVagas(): Observable<Data<Card[]>> {
    const url = 'assets/data/vagas.json';
    const response = this.http.get<Data<Card[]>>(url);
    return response;
  }
}
